# Changes

## 1.0.1 (2021-10-31)

* Add additional `proptest` fuzz test
* Bump CI to build with more rust versions

## 1.0.0 (2021-10-03)

* Expose and use `DEFAULT_VERTEX_CACHE_SIZE` constant
* Add `LICENSE-MIT` and `LICENSE-UNLICENSE` files for convenience
* Updated all dev-dependencies

## 0.3.0 (2021-08-04)

* Breaking change: `order_triangles` now only takes input indices as input.
* Breaking change: `order_vertices` now also returns `Error` instead of strings.
* Added `Config` struct to tweak ordering algorithm via `order_triangles_inplace`.
* Updated `tobj` dev-dependency to v3.1.

## 0.2.0 (2021-07-13)

* Breaking change: Added `Error` and have public function return that instead of strings.
* Updated `tobj` dev-dependency.
* Generate `tarpaulin` coverage reports in CI.


## 0.1.0 (2021-07-08)

* Initial release with `order_triangles` and `order_vertices`.