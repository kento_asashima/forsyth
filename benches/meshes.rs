use std::convert::TryInto;

extern crate tobj;

#[allow(unused)]
pub(crate) fn generate_mesh_indices(obj_file: &str) -> Vec<u32> {
    let (models, _) = tobj::load_obj(
        &obj_file,
        &tobj::LoadOptions {
            single_index: true,
            triangulate: true,
            ignore_lines: true,
            ignore_points: true,
        },
    )
    .expect("Failed to load obj");

    let mut num_ignored_indices = 0;
    let mut indices = Vec::with_capacity(4096);

    for mut m in models {
        let mesh = &mut m.mesh;

        indices.append(&mut mesh.indices);
    }

    println!(
        "Generated {} (+{}) indices from {}",
        indices.len(),
        num_ignored_indices,
        obj_file
    );

    indices
}

#[allow(unused)]
pub(crate) fn synthesize_vertex_buffer<Vertex, Index, Callback>(
    indices: &[Index],
    generator: Callback,
) -> Vec<Vertex>
where
    Callback: Fn(usize) -> Vertex,
    Index: Copy + TryInto<usize>,
{
    if indices.is_empty() {
        return Vec::new();
    }

    let max = indices
        .iter()
        .map(|idx| (*idx).try_into().unwrap_or(0))
        .max()
        .unwrap_or(0);
    if max == 0 {
        return Vec::new();
    }

    let mut vertices = Vec::with_capacity(max + 1);
    for v in 0..max + 1 {
        vertices.push(generator(v));
    }
    vertices
}

#[cfg(test)]
mod tests {

    #[test]
    fn vertex_generation() {
        {
            assert_eq!(
                crate::synthesize_vertex_buffer(&[0, 1, 2, 3], |v| 1000 + v),
                [1000, 1001, 1002, 1003]
            );
        }
    }
}
