#[allow(unused)]
pub(crate) fn merge_vertex(verts: &mut Vec<(i32, i32, i32)>, new_vert: (i32, i32, i32)) -> usize {
    for (i, v) in verts.iter().enumerate().rev() {
        if v == &new_vert {
            return i;
        }
    }
    verts.push(new_vert);
    verts.len() - 1
}

#[allow(unused)]
pub(crate) fn make_quad_patch(width: usize, height: usize) -> Vec<u32> {
    let mut verts = Vec::with_capacity(width * height * 4);
    let mut indices = Vec::with_capacity(width * height * 6);

    let ofs = [(0, 0), (1, 0), (1, 1), (0, 0), (1, 1), (0, 1)];

    for x in 0..width {
        for y in 0..height {
            for o in &ofs {
                indices.push(merge_vertex(&mut verts, (x as i32 + o.0, y as i32 + o.1, 0)) as u32);
            }
        }
    }

    indices
}

#[allow(unused)]
pub(crate) fn make_fan(num_tris: usize) -> Vec<u32> {
    let mut fan = Vec::with_capacity(3 * num_tris);
    let mut vert = 1;
    for _ in 0..num_tris {
        fan.push(0);
        fan.push(vert);
        vert += 1;
        fan.push(vert + 1);
    }
    fan
}

#[allow(unused)]
pub(crate) fn make_multi_fan(num_tris_per_fan: usize, num_fans: usize) -> Vec<u32> {
    let mut out = Vec::with_capacity(num_tris_per_fan * num_fans * 3);

    let fan = make_fan(num_tris_per_fan);
    for _ in 0..num_fans {
        let ofs = out.len() as u32;
        for v in &fan {
            out.push(ofs + *v);
        }
    }
    out
}

#[allow(unused)]
pub(crate) fn is_permutated_mesh(lhs: &[u32], rhs: &[u32]) -> Result<(), String> {
    if lhs.len() != rhs.len() {
        return Err(format!(
            "Length is not equal: {} != {}",
            lhs.len(),
            rhs.len()
        ));
    }

    let mut lhs = Vec::from(lhs);
    let mut rhs = Vec::from(rhs);

    lhs.sort_unstable();
    rhs.sort_unstable();

    if lhs != rhs {
        return Err("Index sets are not the same!".to_owned());
    }

    Ok(())
}
