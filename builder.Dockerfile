FROM rust:latest

RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-get update --yes && \
    apt-get install --yes \
    cmake \
    build-essential \
    gnuplot \
    valgrind

RUN rustup toolchain install stable nightly
RUN rustup component add clippy rustfmt
RUN cargo install cargo-fuzz
RUN cargo install cargo-tarpaulin

WORKDIR /builds/kento_asashima/forsyth

COPY . .
RUN cargo fetch