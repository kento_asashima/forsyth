#![no_main]
use forsyth::{order_triangles, order_triangles_inplace, Config, DEFAULT_VERTEX_CACHE_SIZE};
use libfuzzer_sys::fuzz_target;
use std::convert::TryInto;

fn run_target(data: &[u8]) {
    let prim = data.get(1).unwrap_or(&0);
    if prim % 4 == 0 {
        let indices = data.get(2..).unwrap_or(&[]);
        let result = order_triangles(indices);
        match result {
            Ok(ordered) => {
                if ordered.len() != indices.len() {
                    panic!("Ordered result is not same length");
                }
            }
            _ => (),
        }
    } else if prim % 4 == 1 {
        let data = data.get(1..).unwrap_or(&[]);
        let mut converted = Vec::new();
        for idx in 0..data.len() / 2 {
            if let Some(elem) = data.get((idx * 2)..(idx * 2) + 2) {
                converted.push(u16::from_be_bytes(elem.try_into().unwrap()));
            }
        }
        let result = order_triangles(converted.as_slice());
        match result {
            Ok(ordered) => {
                if ordered.len() != converted.len() {
                    panic!("Ordered result is not same length");
                }
            }
            _ => (),
        }
    } else if prim % 4 == 2 {
        let data = data.get(1..).unwrap_or(&[]);
        let cache_size: u16 = data
            .get(0..2)
            .map(|bytes| u16::from_be_bytes(bytes.try_into().unwrap()))
            .unwrap_or(DEFAULT_VERTEX_CACHE_SIZE);
        let data = data.get(2..).unwrap_or(&[]);
        let mut config = Config::default();
        config.valence_boost_scale = data
            .get(0..4)
            .map(|bytes| f32::from_be_bytes(bytes.try_into().unwrap()))
            .unwrap_or(config.valence_boost_scale);
        config.valence_boost_power = data
            .get(4..8)
            .map(|bytes| f32::from_be_bytes(bytes.try_into().unwrap()))
            .unwrap_or(config.valence_boost_power);
        config.last_tri_score = data
            .get(8..12)
            .map(|bytes| f32::from_be_bytes(bytes.try_into().unwrap()))
            .unwrap_or(config.last_tri_score);
        config.cache_decay_power = data
            .get(12..16)
            .map(|bytes| f32::from_be_bytes(bytes.try_into().unwrap()))
            .unwrap_or(config.cache_decay_power);

        let data = data.get(16..).unwrap_or(&[]);
        let mut converted = Vec::new();
        for idx in 0..data.len() / 2 {
            if let Some(elem) = data.get((idx * 2)..(idx * 2) + 2) {
                converted.push(i16::from_be_bytes(elem.try_into().unwrap()));
            }
        }
        let orig_len = converted.len();
        let result = order_triangles_inplace(config, converted.as_mut_slice(), cache_size);
        match result {
            Ok(ordered) => {
                if ordered.len() != orig_len {
                    panic!("Ordered result is not same length");
                }
            }
            _ => (),
        }
    } else {
        let data = data.get(1..).unwrap_or(&[]);
        let mut converted = Vec::new();
        for idx in 0..data.len() / 2 {
            if let Some(elem) = data.get((idx * 2)..(idx * 2) + 2) {
                converted.push(i16::from_be_bytes(elem.try_into().unwrap()));
            }
        }
        let result = order_triangles(converted.as_slice());
        match result {
            Ok(ordered) => {
                if ordered.len() != converted.len() {
                    panic!("Ordered result is not same length");
                }
            }
            _ => (),
        }
    }
}

fuzz_target!(|data: &[u8]| { run_target(data) });
